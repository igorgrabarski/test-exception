package tech.javarian.tryexception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@SpringBootApplication
public class TryExceptionApplication {

    public static void main(String[] args) {
        SpringApplication.run(TryExceptionApplication.class, args);
    }

}


@RestController
class MyController{
    @GetMapping
    public String foo() throws MyException {
        throw new MyException();
    }
}

class MyException extends Exception {

    @Override
    public String getMessage() {
        return "This is my exception thrown";
    }
}

@RestControllerAdvice
class MyAdvice {

    @ExceptionHandler(MyException.class)
    public void myHandler(MyException exception){
        System.out.println("<=== " + exception.getMessage() + " ===>");
    }
}
